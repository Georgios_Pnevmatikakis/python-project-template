from os import walk, path
from setuptools import setup, find_packages

RESOURCES_DIR = 'resources'
SOURCE_DIR = 'src'
TEST_DIR = 'test'

data_files = []
for root, dirs, files in walk(RESOURCES_DIR):
    data_files.append((path.relpath(root, RESOURCES_DIR), [path.join(root, f) for f in files]))

setup(
    name="Project Name",
    version="0.1",
    packages=find_packages(where=SOURCE_DIR, exclude=(RESOURCES_DIR,)),
    package_dir={"": SOURCE_DIR},
    data_files=data_files,

    entry_points={
        'console_scripts': [
            'project.py = main:main',
        ],
    },

    install_requires=[
        'pytest>=5.3.1',
        'pytest-cov>=2.8.1',
    ],
)
