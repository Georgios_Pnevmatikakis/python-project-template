#!/usr/bin/env bash
python3 -m pytest --cov-report term-missing:skip-covered --cov-branch --cov-fail-under 50 --cov=src test